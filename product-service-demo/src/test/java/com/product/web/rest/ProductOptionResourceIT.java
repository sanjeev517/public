package com.product.web.rest;

import static com.product.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import com.product.Application;
import com.product.model.Product;
import com.product.model.ProductOption;
import com.product.repo.ProductOptionRepository;
import com.product.repo.ProductRepository;
import com.product.rest.ProductOptionResource;
import com.product.web.rest.errors.ExceptionTranslator;

/**
 * Integration tests for the {@link ProductOptionResource} REST controller.
 */
@SpringBootTest(classes = Application.class, properties={"spring.liquibase.enabled=false"})
@RunWith(SpringRunner.class)
public class ProductOptionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ProductOptionRepository productOptionRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    

    @Autowired
    private ProductRepository productRepository;

//    @Autowired
    private ExceptionTranslator exceptionTranslator = new ExceptionTranslator();

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProductOptionMockMvc;

    private ProductOption productOption;
	private Product product;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProductOptionResource productOptionResource = new ProductOptionResource(productOptionRepository, productRepository);
        this.restProductOptionMockMvc = MockMvcBuilders.standaloneSetup(productOptionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductOption createEntity(EntityManager em) {
        ProductOption productOption = new ProductOption()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return productOption;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductOption createUpdatedEntity(EntityManager em) {
        ProductOption productOption = new ProductOption()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        return productOption;
    }

    @Before
    public void initTest() {
    	product = ProductResourceIT.createEntity(em);
        productOption = createEntity(em);
    }

//  @Test
//  @Transactional  
//  public void getAllProductOptions() throws Exception {
//  // Initialize the database
//  productRepository.saveAndFlush(product);
//  List<Product> products =  productRepository.findAll();
//  System.out.println(products.size());
//  ProductOption opt = productOptionRepository.findByProductIdAndOptionId(123l, products.get(0).getId());
//  System.out.println(opt.getName());
//  // Get all the productOptionList
////  restProductOptionMockMvc.perform(get("/api/product-options?sort=id,desc"))
////      .andExpect(status().isOk())
////      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
////      .andExpect(jsonPath("$.[*].id").value(hasItem(productOption.getId().intValue())))
////      .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
////      .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
//}
    
    @Test
    @Transactional
    public void createProductOption() throws Exception {
        int databaseSizeBeforeCreate = productOptionRepository.findAll().size();
        productRepository.saveAndFlush(product);
            
        // Create the ProductOption
        restProductOptionMockMvc.perform(post("/products/{id}/options", product.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productOption)))
            .andExpect(status().isCreated());

        // Validate the ProductOption in the database
        List<ProductOption> productOptionList = productOptionRepository.findAll();
        assertThat(productOptionList).hasSize(databaseSizeBeforeCreate + 1);
        ProductOption testProductOption = productOptionList.get(productOptionList.size() - 1);
        assertThat(testProductOption.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProductOption.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createProductOptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productOptionRepository.findAll().size();
        productRepository.saveAndFlush(product);
        // Create the ProductOption with an existing ID
        productOption.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductOptionMockMvc.perform(post("/products/{id}/options", product.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productOption)))
            .andExpect(status().isBadRequest());

        // Validate the ProductOption in the database
        List<ProductOption> productOptionList = productOptionRepository.findAll();
        assertThat(productOptionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = productOptionRepository.findAll().size();
        productRepository.saveAndFlush(product);
        // set the field null
        productOption.setName(null);

        // Create the ProductOption, which fails.

        restProductOptionMockMvc.perform(post("/products/{id}/options", product.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productOption)))
            .andExpect(status().isBadRequest());

        List<ProductOption> productOptionList = productOptionRepository.findAll();
        assertThat(productOptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProductOptions() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
//        productOptionRepository.saveAndFlush(productOption);

        restProductOptionMockMvc.perform(post("/products/{id}/options", product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(productOption)));
        // Get all the productOptionList
        restProductOptionMockMvc.perform(get("/products/{id}/options", product.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(notNullValue()))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getProductOption() throws Exception {

        productRepository.saveAndFlush(product);
        // create a productOption
        restProductOptionMockMvc.perform(post("/products/{id}/options", product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(productOption)));
        
        Long optionId = product.getOptions().get(0).getId();
        // Get the productOption
        restProductOptionMockMvc.perform(get("/products/{id}/options/{optionId}", product.getId(), optionId))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(optionId))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    public void getNonExistingProductOption() throws Exception {
        // Get the productOption
        restProductOptionMockMvc.perform(get("/products/{id}/options/{optionId}", product.getId(), Long.MAX_VALUE,123))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductOption() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        //saving the data.
        restProductOptionMockMvc.perform(post("/products/{id}/options", product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(productOption)));
        
        Long optionId = product.getOptions().get(0).getId();

        // Update the productOption
        ProductOption updatedProductOption = new ProductOption()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        updatedProductOption.setId(optionId);

        restProductOptionMockMvc.perform(put("/products/options")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductOption)))
            .andExpect(status().isOk());

        // Validate the ProductOption in the database
        List<ProductOption> productOptionList = productOptionRepository.findAll();
        ProductOption testProductOption = productOptionList.get(productOptionList.size() - 1);
        assertThat(testProductOption.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProductOption.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingProductOption() throws Exception {
        int databaseSizeBeforeUpdate = productOptionRepository.findAll().size();
        productOption.setId(Long.MAX_VALUE);
        // Create the ProductOption

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductOptionMockMvc.perform(put("/products/options")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productOption)))
            .andExpect(status().isBadRequest());

        // Validate the ProductOption in the database
        List<ProductOption> productOptionList = productOptionRepository.findAll();
        assertThat(productOptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductOption() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        //saving the data.
        restProductOptionMockMvc.perform(post("/products/{id}/options", product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(productOption)));
        
        Long optionId = product.getOptions().get(0).getId();

        int databaseSizeBeforeDelete = productOptionRepository.findAll().size();

        // Delete the productOption
        restProductOptionMockMvc.perform(delete("/products/options/{id}", optionId)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductOption> productOptionList = productOptionRepository.findAll();
        assertThat(productOptionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
