package com.product.repo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.product.model.ProductOption;


/**
 * Spring Data  repository for the ProductOption entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductOptionRepository extends JpaRepository<ProductOption, Long> {

	@Query("FROM ProductOption AS po WHERE po.product.id=:pId AND po.id= :oId")
	ProductOption findByProductIdAndOptionId(@Param("pId")Long productId, @Param("oId")Long optionId);
}
