package com.product.web.rest.errors;


import java.net.URI;

public class BadRequestAlertException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    public static final URI DEFAULT_TYPE = URI.create("problem-with-message");

    private final String entityName;

    private final String errorKey;

    public BadRequestAlertException(String defaultMessage, String entityName, String errorKey) {
        super( defaultMessage);
        this.entityName=entityName;
        this.errorKey=errorKey;
    }

    

    public String getEntityName() {
        return entityName;
    }

    public String getErrorKey() {
        return errorKey;
    }

  
}
