/**
 * View Models used by Spring MVC REST controllers.
 */
package com.product.web.rest.vm;
