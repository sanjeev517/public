package com.product.web.rest.errors;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;



@ControllerAdvice
public class ExceptionTranslator extends ResponseEntityExceptionHandler {

    /**
     * Post-process the Problem payload to add the message key for the front-end if needed.
     */
    
    @ExceptionHandler(BadRequestAlertException.class)
    public void springHandleNotFound(BadRequestAlertException ex, HttpServletResponse response) throws IOException {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.sendError(HttpStatus.BAD_REQUEST.value(),ex.getMessage());
    }
    
}
