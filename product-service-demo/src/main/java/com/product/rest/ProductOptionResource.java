package com.product.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.product.model.Product;
import com.product.model.ProductOption;
import com.product.repo.ProductOptionRepository;
import com.product.repo.ProductRepository;
import com.product.web.rest.errors.BadRequestAlertException;
import com.product.web.rest.vm.HeaderUtil;

/**
 * REST controller for managing {@link com.product.domain.ProductOption}.
 */
@RestController
@RequestMapping("/products")
@Transactional
public class ProductOptionResource {

    private final Logger log = LoggerFactory.getLogger(ProductOptionResource.class);

    private static final String ENTITY_NAME = "productOption";

    @Value("${spring.application.name}")
    private String applicationName;

    private final ProductOptionRepository productOptionRepository;

    private final ProductRepository productRepository;
    
    public ProductOptionResource(ProductOptionRepository productOptionRepository, ProductRepository productRepository) {
        this.productOptionRepository = productOptionRepository;
        this.productRepository = productRepository;
    }

    /**
     * {@code POST  /product-options} : Create a new productOption.
     *
     * @param productOption the productOption to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productOption, or with status {@code 400 (Bad Request)} if the productOption has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/{id}/options")
    public ResponseEntity<ProductOption> createProductOption(@PathVariable Long id, @Valid @RequestBody ProductOption productOption) throws URISyntaxException {
        log.debug("REST request to save ProductOption : {}", productOption);
        if (productOption.getId() != null) {
            throw new BadRequestAlertException("A new productOption cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<Product>  parent = productRepository.findById(id);
        if(!parent.isPresent()) {
        	throw new BadRequestAlertException("Product doesn't exist with specified Id", ENTITY_NAME, id.toString());
        }
        Product product = parent.get();
        product.addProduct(productOption);
        ProductOption result = productOptionRepository.save(productOption);
        return ResponseEntity.created(new URI("products/options/"+result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                 .body(result);
    }

    /**
     * {@code PUT  /product-options} : Updates an existing productOption.
     *
     * @param productOption the productOption to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productOption,
     * or with status {@code 400 (Bad Request)} if the productOption is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productOption couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/options")
    public ProductOption updateProductOption(@Valid @RequestBody ProductOption productOption) throws URISyntaxException {
        log.debug("REST request to update ProductOption : {}", productOption);
        if (productOption.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        return productOptionRepository.findById(productOption.getId()).map(option -> {
            option.setDescription(productOption.getDescription());
            option.setId(productOption.getId());
            option.setName(productOption.getName());
            return productOptionRepository.save(option);
        }).orElseThrow(()->new BadRequestAlertException("Product Option not available", ENTITY_NAME, productOption.getId()+""));
    }



    /**
     * {@code GET  /product-options/:id} : get the "id" productOption.
     *
     * @param id the id of the productOption to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productOption, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/options/{id}")
    public ResponseEntity<ProductOption> getProductOption(@PathVariable Long id) {
        log.debug("REST request to get ProductOption : {}", id);
        Optional<ProductOption> productOption = productOptionRepository.findById(id);
        return productOption.map(p -> ResponseEntity.ok().body(p)).orElse(ResponseEntity.notFound().build());
    }

    /**
     * {@code DELETE  /options/:id} : delete the "id" productOption.
     *
     * @param id the id of the productOption to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/options/{optionid}")
    public ResponseEntity<Void> deleteProductOption( @PathVariable Long optionid) {
        log.debug("REST request to delete ProductOption : {}", optionid);
        productOptionRepository.deleteById(optionid);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, optionid.toString())).build();
    }
    
    
    /**
     * {@code GET  /{id}/options} : get all the productOptions.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productOptions in body.
     */
    
    @GetMapping("/{id}/options")
    public ResponseEntity<List<ProductOption>> getAllProductOptions(@PathVariable Long id) {
        log.debug("REST request to get all ProductOptions with Product id {}", id);
        Optional<Product> product = productRepository.findById(id);
        return  product.map( p -> ResponseEntity.ok().body(p.getOptions())).orElse(ResponseEntity.notFound().build());
    }
    
    @GetMapping("/{id}/options/{optionId}")
    public ProductOption getProductOptions(@PathVariable("id") Long id, @PathVariable("optionId") Long optionId) {
        log.debug("REST request to get all ProductOptions with Product id {}", id);
        Optional<ProductOption>  productOption = productOptionRepository.findById(optionId);
        if(productOption.isPresent() && productOption.get().getProduct().getId().equals(id)) {
        	return productOption.get();
        }else{
           throw new BadRequestAlertException("Invalid id", ENTITY_NAME, id.toString());
        }
    }
    
}
