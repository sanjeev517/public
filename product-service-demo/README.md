This Application has been made as possible as close to prod env. 
The application has more then 95% code coverage using integration test cases. The seed data has been supplied as part of development. 

It is a spring boot based application and following technology stack being used
a. Spring Data with JPA 2.0 Specifications
b. Swagger API documentation
c. Liquibase for managing the database change-log and 
d. Logback for logging framework
e. H2 database disk-mode but can be migrated to any full DB server such as Postgres.
h. Maven for build process

Steps to build and run the application.
Pre-requisites
a. Java 8
b. Maven 2.+  

Steps to build 
a. Build command : mvn clean install
b. Running the Built application(prod): java -jar target/product-1.0.0.jar --spring.liquibase.change-log="classpath:db/master.xml"
this command will start the service apply the DB changelog.
c. Running the application in dev using spring-boot plugin: mvn spring-boot:run

Swagger documentation URL: 
http://localhost:8080/swagger-ui.html

If you want to see the DB structure
H2-database console:
http://localhost:8080/h2-console
JDBC URL:jdbc:h2:file:./target/h2db/db/product;DB_CLOSE_DELAY=-1
Username: sa
password: password

Feel free to reach me on contacttosanjeev@gmail.com if you find any issue to run this application.