package logic;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import org.junit.Test;

import model.Employee;

public class EmployeeHirerchyServiceTest {

    @Test
    public void parseAndReturnCEOEmployeeTest(){
        
        List<String> data = new ArrayList<>();
        data.add("test,1,5");
        data.add("test2,2,5");
        data.add("test3,5,");
        EmployeeHirerchyService service = new EmployeeHirerchyService(data);
        Employee ceo = service.getCeoEmployee();
        assertEquals("test3",ceo.getName());
        assertEquals(5, ceo.getId());
        assertNull(ceo.getManager());
        
        assertEquals(2, ceo.getDirectSubordinates().size());
        assertTrue(ceo.getDirectSubordinates().contains(new Employee("test",1)));
        assertTrue(ceo.getDirectSubordinates().contains(new Employee("test2",2)));
    }
    
    
    @Test
    public void testWithQuestionData(){
        List<String> data = getInitData();
        EmployeeHirerchyService service = new EmployeeHirerchyService(data);
        Employee ceo = service.getCeoEmployee();
        assertEquals("Jamie",ceo.getName());
        assertEquals(150, ceo.getId());
        assertNull(ceo.getManager());
        
        assertEquals(2, ceo.getDirectSubordinates().size());
        assertTrue(ceo.getDirectSubordinates().contains(new Employee("Allan",100)));
        assertTrue(ceo.getDirectSubordinates().contains(new Employee("Steve",400)));
    }
    
    
    
    @Test
    public void verifyOutput(){
        
        List<String> data = getInitData();
        String expected = 
                "\nJamie(150)\n\n" +
                "\tAllan(100)\n\n" +
                "\t\tMartin(220)\n\n" +
                "\t\tAlex(275)\n\n" +
                "\tSteve(400)\n\n" +
                "\t\tDavid(190)\n";
        System.out.println(expected);
        EmployeeHirerchyService service = new EmployeeHirerchyService(data);
        assertEquals(expected, service.generateFormatedString(service.getCeoEmployee()));
    }

    @Test
    public void testInvalidRecord(){
        List<String> data = getInitData();
        data.add("sanjeev,45,0");
        EmployeeHirerchyService service = new EmployeeHirerchyService(data);
        assertTrue(service.getInvalidReocords().contains("sanjeev,45,0"));
    }
    

    private List<String> getInitData() {
        List<String> data = new ArrayList<>();
        data.add("Allan,100,150");
        data.add("Martin,220,100");
        data.add("Alex,275,100");
        data.add("Steve,400,150");
        data.add("David,190,400");
        data.add("Jamie,150,");
        return data;
    }
    

    
    
    
    
    
}
