package app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import logic.EmployeeHirerchyService;
import model.Employee;

public class Application {

    public static void main(String[] args) throws IOException {
       
        List<String> lines = Files.readAllLines(Paths.get("initData.txt"));
        EmployeeHirerchyService service = new EmployeeHirerchyService(lines.subList(1, lines.size()));
        Employee employee = service.getCeoEmployee();
        System.out.println(service.generateFormatedString(employee));
    }

  

}
