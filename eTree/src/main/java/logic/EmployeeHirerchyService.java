package logic;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import model.Employee;

/**
 * Expose API to convert flat records to Employee Tree.
 * Following API
 * a. getCeoEmployee // root employee
 * b. Print Tree
 * c. FilterInvalid records 
 * @author vermas13
 *
 */
public class EmployeeHirerchyService {

    /**
     * intermediate modelObject.
     */
    private Map<Long, List<RawRecord>> mngrIdVsEmpRecords;

    private Employee rootEmployee;

    public EmployeeHirerchyService(List<String> lines) {
        parseFlatRecordsAndPopulateIntermediateModel(lines);
        rootEmployee = constructCompleteEmployeeTree();
    }

    public Employee getCeoEmployee() {
        return rootEmployee;
    }

    public String generateFormatedString(Employee root) {
        return formatNode(root, 0);
    }

    /**
     * Records having invalid manager-ID. No employee exist with given manager id.
     * 
     * @return
     */
    public List<String> getInvalidReocords() {
        return mngrIdVsEmpRecords.entrySet().stream().map(e -> e.getValue())
                .flatMap(List::stream)
                .map(v -> v.toString())
                .collect(Collectors.toList());

    }

    /**
     *
     * @param take
     *            date in csv format
     * @return root object of employee.
     */
    private void parseFlatRecordsAndPopulateIntermediateModel(List<String> lines) {
        try {
            mngrIdVsEmpRecords = lines.stream().map(s -> new RawRecord(s))
                    .collect(Collectors.groupingBy(RawRecord::getManagerId));
        } catch (Exception e) {
            throw new RuntimeException("Parsing failed", e);
        }

    }

    /**
     * Take the manger Map and return root Employee object
     * 
     * @param mngrMap
     * @return
     */
    private Employee constructCompleteEmployeeTree() {
        Employee ceo = constructCEONode();
        constructTreeForNode(ceo);
        return ceo;
    }

    private Employee constructCEONode() {
        RawRecord ceoRecord = mngrIdVsEmpRecords.remove(Employee.CEO_MGR_ID).get(0);
        Employee ceo = new Employee(ceoRecord.getName(), ceoRecord.getId());
        return ceo;
    }

    private void constructTreeForNode(Employee manager) {
        List<RawRecord> subordRecords = mngrIdVsEmpRecords.remove(manager.getId());
        if (subordRecords == null) {
            return;
        }

        List<Employee> subordinates = subordRecords.stream().map(r -> {
            Employee subordiante = new Employee(r.getName(), r.getId());
            subordiante.setManager(manager);
            return subordiante;
        }).collect(Collectors.toList());
        manager.setDirectSubordinates(subordinates);
        // call recursively
        for (Employee employee : subordinates) {
            constructTreeForNode(employee);
        }

    }

    private String formatNode(Employee root, int tab) {
        String output = "\n";
        for (int i = 0; i < tab; i++) {
            output = output + "\t";
        }
        output = output + root.toString() + "\n";
        tab++;
        List<Employee> sub = root.getDirectSubordinates();
        if (sub == null)
            return output;
        for (Employee employee : sub) {
            output = output + formatNode(employee, tab);
        }
        return output;
    }

    /**
     * Intermediate model class parsing the string to model class. Purposefully not exposing as API class.
     * 
     * @author vermas13
     *
     */
    private class RawRecord {
        private final String name;
        private final long id;
        private final long managerId;

        public RawRecord(String record) {
            String[] raw = record.split(",", -1);
            this.id = Long.parseLong(raw[1].trim());
            this.name = raw[0];
            if (raw[2] == null || raw[2].trim().isEmpty()) {
                this.managerId = Employee.CEO_MGR_ID;
            } else {
                this.managerId = Long.parseLong(raw[2].trim());
            }
        }

        public String getName() {
            return name;
        }

        public long getId() {
            return id;
        }

        public long getManagerId() {
            return managerId;
        }

        @Override
        public String toString() {
            return name + "," + id + "," + managerId;
        }
        

    }

}
