Following items covered.
a. Maven based application no external dependency except Junit4
b. Java version 8
c. Covering all three required use cases as part of Junit test cases
	1. Convert flat records to Employee tree where root employee is CEO. Junit parseAndReturnCEOEmployeeTest(), testWithQuestionData()
	2. Print the complete tree.  Junit test verifyOutput()
	3. Filter and report in valid records. junit test testInvalidRecord()
d. app.Application is main class which will take data from csv file and perform all above mentioned operations.

command to run--> mvn clean install
![alt text](report.jpg?raw=true "Output")